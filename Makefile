

IMAGE := nanotrix/ntx:1.5.2-7ae3adc
TESTFILTER ?=.*
all: repo test
	@echo Completed repo build

repo:
	@echo Building index.json
	@docker run --rm -i -v $(CURDIR):/work $(IMAGE) devel task repo build -i taskdef.json -o index.json

.PHONY: test 
test:
	@echo Running tests
	@docker run --rm -i --entrypoint=/usr/bin/perl -w /work -e TESTFILTER=$(TESTFILTER) -e NTX_TOKEN=$(NTX_TOKEN) -v $(CURDIR):/work $(IMAGE) /work/scripts/test

test-force:
	@echo Running tests
	@docker run --rm -i --entrypoint=/usr/bin/perl -w /work -e TESTFILTER=$(TESTFILTER) -e NTX_TOKEN=$(NTX_TOKEN) -v $(CURDIR):/work $(IMAGE) /work/scripts/test --force