1. create ssh keys 
    ```
    ssh-keygen -t rsa -b 4096
    ```
3. copy public key to remote machine
    ```
    ssh-copy-id -p 22 user@testedcomputer 
    ```
2. set token
    ```
        export NTX4_TOKEN=$token
    ```

4. run test
    ```
    perl scripts/cpu.speed test/cpu/large user@testedcomputer:22
    ```